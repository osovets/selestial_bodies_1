package com.company;

class Satellite extends Celestial_Body {
    public Satellite(String name) {
        super(name);
    }

    @Override
    public void display() {
        System.out.printf("Satellite Name: %s \n", super.getName());

    }
}

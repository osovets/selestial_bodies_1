package com.company;

class Planet extends Celestial_Body {

    public Planet(String name) {
        super(name);
    }

    @Override
    public void display() {
        System.out.printf("Planet Name: %s \n", super.getName());

    }
}

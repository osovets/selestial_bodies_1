package com.company;

public abstract class Celestial_Body {
    private String name;
    public String getName(){ return name;
    }
    public Celestial_Body(String name){
        this.name = name;
    }
    public abstract void display();
}

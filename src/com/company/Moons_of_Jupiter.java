package com.company;

public enum Moons_of_Jupiter {

    Io,
    Europe,
    Ganymede,
    Callisto

}
